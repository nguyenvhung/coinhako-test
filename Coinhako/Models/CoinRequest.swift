import Foundation

class CoinRequest: APIRequest {
    var method = RequestType.GET
    var path = "api/v3/price/all_prices_for_mobile?counter_currency=USD"
    var parameters = [String: String]()
    var baseToFilter: String = ""

    init(name: String) {
        if self.method == .POST {
            parameters["name"] = name
        } else {
            baseToFilter = name
        }
    }
}
