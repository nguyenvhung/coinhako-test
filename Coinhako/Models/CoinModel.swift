import Foundation

struct CoinModel: Codable {
    let base: String?
    let name: String?
    let iconUrl: String?
    let buyPriceString: String?
    let sellPriceString: String?

    private enum CodingKeys: String, CodingKey {
        case name = "name"
        case base = "base"
        case iconUrl = "icon"
        case buyPriceString = "buy_price"
        case sellPriceString = "sell_price"
    }
    
    var buyPrice: Double {
        get {
            if let cost = Double(buyPriceString ?? "0.0") {
                return cost
            } else {
                return 0.0
            }
        }
    }

    var sellPrice: Double {
        get {
            if let cost = Double(sellPriceString ?? "0.0") {
                return cost
            } else {
                return 0.0
            }
        }
    }
}

//{
//     "base": "NEO",
//     "counter": "USD",
//     "buy_price": "55.3783",
//     "sell_price": "54.8575",
//     "icon": "https://cdn.coinhako.com/assets/wallet-neo-3fddd91057f65c159dbb601f503d637d144398d0573bf54d69fd6edf389827c7.png",
//     "name": "Neo"
//   }
