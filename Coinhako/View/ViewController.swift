import UIKit
import RxSwift
import RxCocoa
import SafariServices
import Kingfisher
import Material

class ViewController: UIViewController  {
    private let tableView = UITableView()
    private let cellIdentifier = "cellIdentifier"
    private let apiClient = APIClient()
    private let disposeBag = DisposeBag()
    private let vm = ViewModel()
    private var searchController = UISearchController()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureSearchBar()
        configureProperties()
        configureLayout()
        configureReactiveBinding()
    }
    
    private func configureSearchBar() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.placeholder = "Search for coins"
        searchController.searchBar.delegate = self
        self.searchController = searchController
    }

    private func configureProperties() {
        tableView.register(TableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        navigationItem.searchController = searchController
        navigationItem.title = "Coins"
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationController?.navigationBar.prefersLargeTitles = true
    }

    private func configureLayout() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        tableView.contentInset.bottom = view.safeAreaInsets.bottom
    }

    private func configureReactiveBinding() {
        ViewModel.shared.items
            .subscribeOn(MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .bind(to: tableView.rx.items(cellIdentifier: cellIdentifier)) { index, model, cell in
                cell.textLabel?.text = model.name
                cell.detailTextLabel?.text = "Sell: \(model.sellPrice) | Buy: \(model.buyPrice)"
                cell.textLabel?.adjustsFontSizeToFitWidth = true
                
                let url = URL(string: model.iconUrl ?? "")
                cell.imageView?.kf.setImage(with: url)
            }
            .disposed(by: disposeBag)
        
        searchController.searchBar.rx.text.asObservable()
            .map { ($0 ?? "").lowercased() }
            .subscribe(
                onNext: { [weak self] text in
                    guard let _ = self else { return }
                    ViewModel.shared.currentKeyword.accept(text)
                    ViewModel.shared.getListItems(completion: nil)
                }, onError: { [weak self] error in
                    NSLog(error.localizedDescription)
                }, onCompleted: {
                }).disposed(by: disposeBag)
        
        tableView.rx.modelSelected(CoinModel.self)
            .map {
                let url = self.apiClient.detailURL(base: $0.base ?? "")
                return url
            }
            .map { SFSafariViewController(url: $0) }
            .subscribe(onNext: { [weak self] safariViewController in
                safariViewController.modalPresentationStyle = .formSheet
                self?.present(safariViewController, animated: true)
            })
            .disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let self = self else { return }
                self.tableView.deselectRow(at: indexPath, animated: true)
            }).disposed(by: disposeBag)
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        ViewModel.shared.currentKeyword.accept("")
        ViewModel.shared.getListItems(completion: nil)
    }
}
