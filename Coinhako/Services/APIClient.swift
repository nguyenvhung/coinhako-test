import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

protocol APIClientProtocol {
    func fetch(apiRequest: APIRequest, _ completion: @escaping ([CoinModel]?) -> Void)
    func detailURL(base: String) -> URL
}

class APIClient: APIClientProtocol {
    private let baseURL = URL(string: "https://www.coinhako.com/")!
    
    func fetch(apiRequest: APIRequest, _ completion: @escaping ([CoinModel]?) -> Void) {
        var request = apiRequest.request(with: baseURL)
        request.cachePolicy = .returnCacheDataElseLoad
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                NSLog(error.localizedDescription)
                completion(nil)
                return
            }
            if let data = data {
                let json = try? JSON(data: data)
                let array =  try? json?["data"].rawData()
                if let list = array {
                    let models = try? JSONDecoder().decode([CoinModel].self, from: list!)
                    if let request = apiRequest as? CoinRequest, request.baseToFilter != "" {
                        completion( models?.filter { return ($0.base ?? "").lowercased().contains(request.baseToFilter) } )
                    } else {
                        completion(models)
                    }
                } else {
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
        task.resume()
    }
    
    
    func detailURL(base: String) -> URL {
        guard let url = URL(string: "\(baseURL)coins/\(base.uppercased())/USD") else {
            return baseURL
        }
        return url
    }
}
