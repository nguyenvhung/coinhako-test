
//
//  ViewModel.swift
//  Coinhako
//
//  Created by Hung Nguyen on 16/08/2021.
//  Copyright © 2021 Maciej Matuszewski. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class ViewModel {
    
    static var shared = ViewModel()
    
    var currentKeyword: BehaviorRelay<String> =  BehaviorRelay<String>(value: "")
    var items: BehaviorRelay<[CoinModel]> =  BehaviorRelay<[CoinModel]>(value: [])
    var client = APIClient()
    
    func getListItems(completion: (() -> Void)?) {
        let name = currentKeyword.value
        let request = CoinRequest(name: name)
        self.client.fetch(apiRequest: request) { results in
            NSLog("results = \(results)")
            if let res = results {
                self.items.accept(res)
            }
        }
    }
    
}

