import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var taskTimer: Timer?
    private let kTimeoutInSeconds:TimeInterval = 2.0

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.startFetching()
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        self.stopFetching()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        self.startFetching()
    }
    
    private func startFetching() {
        self.taskTimer = Timer.scheduledTimer(timeInterval: self.kTimeoutInSeconds,
                                             target: self,
                                             selector: #selector(getListItemsTimed),
                                             userInfo: nil,
                                             repeats: true)
    }
    private func stopFetching() {
        self.taskTimer?.invalidate()
    }
    
    @objc func getListItemsTimed()  {
        ViewModel.shared.getListItems(completion: nil)
    }
}

