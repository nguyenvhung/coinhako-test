//
//  CoinhakoUnitTests.swift
//  CoinhakoUnitTests
//
//  Created by Hung Nguyen on 17/08/2021.
//  Copyright © 2021 Maciej Matuszewski. All rights reserved.
//

import XCTest
import UIKit
import RxTest
import RxBlocking
import RxSwift

@testable import Coinhako

class CoinhakoUnitTests: XCTestCase {
    var disposeBag: DisposeBag!
    var viewModel: ViewModel!
    var scheduler: ConcurrentDispatchQueueScheduler!
    var testScheduler: TestScheduler!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        viewModel = ViewModel()
        scheduler = ConcurrentDispatchQueueScheduler(qos: .default)
        testScheduler = TestScheduler(initialClock: 0)
    }
    
    override func tearDown() {
        disposeBag = nil
        viewModel = nil
        scheduler = nil
        testScheduler = nil
        super.tearDown()
    }
    
    class MockApiClient: APIClientProtocol {
        func fetch(apiRequest: APIRequest, _ completion: @escaping ([CoinModel]?) -> Void) {
            let tempIconUrl: String = "https://www.vhv.rs/dpng/d/426-4267714_empty-gold-coin-png-gold-coin-icon-png.png"
            let models = [
                CoinModel(base: "BTC", name: "Bitcoin", iconUrl: tempIconUrl, buyPriceString: "65.000", sellPriceString: "70.010"),
                CoinModel(base: "BCT", name: "Bct", iconUrl: tempIconUrl, buyPriceString: "64.000", sellPriceString: "71.010"),
                CoinModel(base: "LTT", name: "Ltt", iconUrl: tempIconUrl, buyPriceString: "63.000", sellPriceString: "79.010"),
                CoinModel(base: "LTT2", name: "Ltt", iconUrl: tempIconUrl, buyPriceString: "63.000", sellPriceString: "79.010"),
                CoinModel(base: "BCT7", name: "Bct", iconUrl: tempIconUrl, buyPriceString: "64.000", sellPriceString: "71.010"),
            ]
            completion(models)
        }
        
        func detailURL(base: String) -> URL {
            return URL(string: "https://coinlist.co/mina")!
        }
    }
    
    func testViewModel() throws {
    }
    
    func testWhenInitialStateAllButtonsAreDisabled() {
        
        viewModel.getListItems(completion: nil)
        
        viewModel.items
            .subscribe(onNext: { list in
                XCTAssert(list.count == 4)
            })
            .disposed(by: self.disposeBag)
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
